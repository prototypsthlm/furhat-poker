package furhatos.app.poker

import furhatos.skills.Skill
import furhatos.flow.kotlin.*
import se.prototyp.furhat.poker.Start

class PokerSkill : Skill() {
    override fun start() {
        userManager.setSimpleEngagementPolicy(6.0, 1)
        Flow().run(Start)
    }
}

fun main(args: Array<String>) {
    Skill.main(args)
}
