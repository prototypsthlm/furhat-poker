package se.prototyp.furhat.poker.deck

import com.google.gson.annotations.SerializedName

data class DrawCardsResponse(val cards: List<CardsItem>?,
                             val success: Boolean = false,
                             @SerializedName("deck_id")
                             val deckId: String = "",
                             val remaining: String = "")

class CardPair constructor(cards: List<CardsItem>?) {
    val computerCard: CardsItem = cards!![0]
    val humanCard: CardsItem = cards!![1]
}
