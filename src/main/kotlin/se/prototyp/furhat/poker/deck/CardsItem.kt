package se.prototyp.furhat.poker.deck

data class CardsItem(
        val image: String = "",
        val code: String = "",
        val suit: String = "",
        val value: String = "") {

    fun toText(): String {
        return this.value + " of " + this.suit
    }

    fun intValue(): Int {
        return when (this.value) {
            "ACE" -> 14
            "KING" -> 13
            "QUEEN" -> 12
            "JACK" -> 11
            "0" -> 10
            else -> this.value.toInt()
        }
    }
}
