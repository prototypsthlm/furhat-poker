package se.prototyp.furhat.poker.deck

import com.google.gson.Gson
import com.mashape.unirest.http.Unirest

const val baseUrl = "https://deckofcardsapi.com/api/deck"

fun draw2Cards(deck: Deck? = null): CardPair {

    val deckId = deck?.deckId ?: "new"

    val resp = Unirest.get("${baseUrl}/$deckId/draw/?count=2")
            .asJson()

    if (resp.status != 200) {
        throw Exception("Something went wrong drawing cards: " + resp.status)
    } else {
        val json = resp.body.toString()
        val drawCardsResponse = Gson().fromJson(json, DrawCardsResponse::class.java)
        return CardPair(drawCardsResponse.cards)
    }
}