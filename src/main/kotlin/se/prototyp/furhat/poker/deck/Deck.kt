package se.prototyp.furhat.poker.deck

data class Deck(val success: Boolean = false,
                val shuffled: Boolean = false,
                val deckId: String = "",
                val remaining: Int = 0)