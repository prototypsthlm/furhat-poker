package se.prototyp.furhat.poker

import se.prototyp.furhat.poker.deck.CardPair
enum class CardComparisonResult {
    CompWin,
    HumanWin,
    Draw
}

class Score {
    val pointsLimit: Int = 10 // First to this score wins

    private var compPoints = 0
    private var humanPoints = 0

    fun getReadScoreStr(): String {
        return "The score is, human: ${humanPoints}, furhat: ${compPoints}"
    }

    fun addToScore(cards: CardPair, multiplier: Int) {
        if (compareCards(cards) == CardComparisonResult.CompWin) {
            compPoints = 1*multiplier
        } else if (compareCards(cards) == CardComparisonResult.HumanWin) {
            humanPoints += 1*multiplier
        }
        // Else values are equal and noone gets points
    }

    fun addPoints(toHuman: Boolean, num: Int) {
        if (toHuman) {
            humanPoints += num
        } else {
            compPoints += num
        }
    }

    fun compareCards(cards: CardPair): CardComparisonResult {
        val compVal = cards.computerCard.intValue()
        val humanVal = cards.humanCard.intValue()

        return when {
            compVal > humanVal -> CardComparisonResult.CompWin
            compVal < humanVal -> CardComparisonResult.HumanWin
            else -> CardComparisonResult.Draw
        }
    }

    fun hasHumanWon(): Boolean {
        return humanPoints >= pointsLimit
    }

    fun hasComputerWon(): Boolean {
        return compPoints >= pointsLimit
    }

    fun reset() {
        compPoints = 0
        humanPoints = 0
    }
}