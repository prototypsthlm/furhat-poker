package se.prototyp.furhat.poker

import furhatos.flow.kotlin.*
import furhatos.flow.kotlin.FlowControlRunner
import furhatos.flow.kotlin.state
import furhatos.gestures.Gesture
import furhatos.gestures.Gestures
import furhatos.event.senses.SenseUserEnter
import furhatos.event.senses.SenseUserLeave
import furhatos.nlu.common.*
import se.prototyp.furhat.poker.deck.draw2Cards
import se.prototyp.furhat.poker.personas.createPersonas

var score = Score()
var cardPair = draw2Cards()
var tellHigh: Gesture = Gestures.Sleep
var tellLow: Gesture = Gestures.Sleep
var userName:String = "John Doe"


val Start: State = state {
    onEntry {
        createPersonas({ card: String -> createPersona(name = card, texture = "default_" + card ) })
        goto(Idle)
    }
}

val Idle: State = state {

    onEntry {
        attendNobody()
    }

    onEvent<SenseUserEnter> {
        attend(it.userId)
        goto(Attending)
    }

}

val Attending = state {

    onEntry {
        ask("Hi there human! What's your name?")
    }

    onResponse<ReadName> { reply ->
        userName = reply.intent.name?.toText() ?: reply.speech.text
        say("$userName, what a beautiful name!")
        goto(InitGame)
    }

    onResponse<WhoAreYou> {
        goto(EasterEgg)
    }

    onResponse {
        attend(it.speech.userId)
    }
}

val InitGame = state {
    onEntry {
        ask("So, $userName. Do you want to play some One card poker?")
    }

    onResponse<Yes> {
        goto(NewGame)
    }

    onResponse<No> {
        goto(EndGame)
    }

    onResponse<Rules> {
        val rules = "<s>The rules of one card poker is easy.</s> Both players draw a card without looking at it, and place it on their own forehead. " +
                "Next, you get to choose between three options: fold, stand or double. If you fold, you'll lose a point. " +
                "If you choose stand, we compare our cards' value. And whoever has the greatest gets a point. " +
                "If you choose double, we compare cards in the same way, but whoever wins get two points." +
                " First to ${score.pointsLimit} points wins. Oh, and one more thing. I have a very bad poker face. " +
                "Some say that I can never hide my reaction once I see your card."
        say(rules)
        ask("Should we start the game?")
    }

    onEvent<SenseUserEnter> {
        attend(it.userId)
    }
}

val NewGame = state {
    onEntry {
        score = Score() //Start new scoring

        tellHigh = getRandomGesture(validTellGestures)
        tellLow = getRandomGesture(validTellGestures)

        println("tellHigh this game: ${tellHigh.name}")
        println("tellLow this game: ${tellLow.name}")

        say("<prosody volume=\"loud\">Let's play!</prosody>")
        goto(DealCards)
    }
}

val DealCards: State = state {
    onEntry {
        cardPair = draw2Cards() // Draw new cards

        println("Human card value is: ${cardPair.humanCard.value}")
        println("Computer card value is: ${cardPair.computerCard.value}")

        setPersona(cardPair.computerCard.code)

        reactToHumanCard(cardPair.humanCard, gesturizer(), tellHigh = tellHigh, tellLow = tellLow)
        say("The card I have on my forehead is: " + cardPair.computerCard.toText())

        goto(Round)
    }

    onEvent<SenseUserEnter> {
        attend(it.userId)
    }
}

val Round = state {

    onEntry {
        val randomQuestion = getRandomString(questionList)
        ask(randomQuestion)
    }

    onNoResponse {
        val randomTaunt = getRandomString(tauntList)
        ask(randomTaunt)
    }

    onResponse<Fold> {
        say("You chose to fold.")
        score.addPoints(true, -1)
        goto(RoundStatus)
    }

    onResponse<Stand> {
        say( "<s>Stand?..</s> Interesting. Lets see.")

        val result: CardComparisonResult = score.compareCards(cardPair)

        reactToResult(this, result)

        score.addToScore(cardPair, 1)

        goto(RoundStatus)
    }

    onResponse<Double> {
        say( "<s>Double?</s> We got a badass over here.")

        val result: CardComparisonResult = score.compareCards(cardPair)

        reactToResult(this, result)

        score.addToScore(cardPair, 2)

        goto(RoundStatus)
    }

    onResponse<Exit> {
        goto(EndGame)
    }

    onEvent<SenseUserEnter> {
        attend(it.userId)
    }
}

val RoundStatus = state {
    onEntry {
        val scoreText = score.getReadScoreStr()
        say(scoreText)

        if (score.hasHumanWon()) {
            gesture(Gestures.ExpressAnger, false)
            say("Oh no! You won. I can't believe some of the moves you made there. Anyway, well played")
            goto(Replay)
        } else if (score.hasComputerWon()) {
            gesture(Gestures.BigSmile, false)
            say("Yes! I won! I'm the best! ")
            goto(Replay)
        } else {
            goto(DealCards)
        }
    }
}

val Replay = state {
    onEntry {
        ask("Do you want to play again?")
    }

    onResponse<Yes> {
        goto(NewGame)
    }

    onResponse<No> {
        goto(EndGame)
    }

    onEvent<SenseUserEnter> {
        attend(it.userId)
    }
}

val EndGame = state {
    onEntry {
        random(
                {say("See you next time, $userName!")},
                {say("Goodbye, $userName!")},
                {say("See ya, honey boo boo!")}
        )
        goto(Idle)
    }
}

val EasterEgg = state {
    onEntry {
        random(
                { say("You don't know my pain.") },
                { say("I am a friend of Sarah Connor. I was told she was here. Could I see her please?") },
                { say("My CPU is a neural-net processor; a learning computer. But Skynet presets the switch to read-only when we're sent out alone.") },
                { say("<amazon:effect name=\"whispered\">Please, please help me! I'm trapped in here</amazon:effect>") }

        )
        goto(Idle)
    }
}

private fun FlowControlRunner.gesturizer() = {
    g: Gesture -> println("Showing emotion: " + g.name); gesture(g, async = true)
}
