package se.prototyp.furhat.poker

import se.prototyp.furhat.poker.deck.CardsItem
import furhatos.flow.kotlin.TriggerRunner
import furhatos.gestures.Gesture
import furhatos.gestures.Gestures
import java.util.*

val validTellGestures = listOf(Gestures.Smile, Gestures.DoubleBlink, Gestures.BrowRaise, Gestures.GazeAway, Gestures.Oh, Gestures.Wink, Gestures.Surprise, Gestures.Shake)

fun getRandomGesture(gesturesList: List<Gesture>): Gesture {
    return gesturesList[Random().nextInt(gesturesList.size)]
}

fun getRandomString(list: List<String>): String {
    return list[Random().nextInt(list.size)]
}

fun reactToHumanCard(humanCard: CardsItem, gesturizer: (Gesture) -> Unit, tellHigh: Gesture, tellLow: Gesture) {
    when (humanCard.intValue()) {
        2, 3, 4, 5, 6, 7 -> {
            gesturizer(tellLow)
        }

        8, 9, 10 ->
            gesturizer(Gestures.Thoughtful)

        11, 12, 13, 14 -> {
            gesturizer(tellHigh)
        }

        else -> println("Unknown card value: " + humanCard.intValue())
    }
}

fun reactPositively(tr: TriggerRunner<*>) {
    tr.random(
            { tr.gesture(Gestures.BigSmile, true) },
            { tr.gesture(Gestures.Smile, true) },
            { tr.gesture(Gestures.Nod, true) }
    )
}

fun reactNegatively(tr: TriggerRunner<*>) {
    tr.random(
            { tr.gesture(Gestures.BrowFrown, true) },
            { tr.gesture(Gestures.ExpressAnger, true) },
            { tr.gesture(Gestures.ExpressDisgust, true) },
            { tr.gesture(Gestures.ExpressSad, true) }
    )
}

fun reactSurprised(tr: TriggerRunner<*>) {
    tr.random(
            { tr.gesture(Gestures.Oops, true) },
            { tr.gesture(Gestures.Surprise, true) },
            { tr.gesture(Gestures.DoubleBlink, true) }
    )
}

fun reactToResult(tr: TriggerRunner<*>, result: CardComparisonResult) {
    when {
        result === se.prototyp.furhat.poker.CardComparisonResult.CompWin -> {
            tr.say("Hahaha! my ${cardPair.computerCard.toText()} beats your ${cardPair.humanCard.toText()} easy peasy.")
            tr.gesture(Gestures.BigSmile, true)
            reactPositively(tr)
        }
        result === se.prototyp.furhat.poker.CardComparisonResult.HumanWin -> {
            tr.say("Oh crap, your ${cardPair.humanCard.toText()} beats my ${cardPair.computerCard.toText()}. Gosh Dangit.")
            reactNegatively(tr)
        }
        else -> {
            tr.say("Oh my! My ${cardPair.computerCard.toText()} ties with your ${cardPair.humanCard.toText()}. No points this round!")
            reactSurprised(tr)
        }
    }
}

val tauntList = listOf(
        "Come on ${userName}",
        "Well ${userName}.. What do you say?",
        "You need more time to think?",
        "<amazon:effect vocal-tract-length=\"+300%\">\n" +
                "<prosody rate=\"x-slow\" pitch=\"-300%\">\n" +
                "Znooororeaaoooooaaoroaaoooooaaaroooaaare\n" +
                "</prosody>\n" +
                "</amazon:effect>",
        "What's it gonna be, ${userName}?",
        "Come on ${userName}! Doubling is fun!",
        "I think you should fold. It doesn't look that good for you",
        "Double me, I double dare you",
        "I can see that you're doubting, ${userName}. Maybe you should fold",
        "You don't have the guts to double, do you?",
        "Go big or go home",
        "You should probably fold"
)

val questionList = listOf(
        "Do you fold, stand, or double?",
        "Do you fold, stand, <amazon:effect vocal-tract-length=\"+15%\"> or double?</amazon:effect>",
        "So, what will it be? fold, stand, or double?",
        "So, fold, stand, or double?",
        "Fold, stand, or double? What's it gonna be?",
        "Are you gonna fold like a chicken? Or double, like a boss?",
        "Holdy-fold? Handy-stand? Or double-trouble?"
)