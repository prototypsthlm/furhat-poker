package se.prototyp.furhat.poker

import furhatos.nlu.Intent
import furhatos.nlu.common.PersonName
import furhatos.util.Language

class Fold : Intent() {
    override fun getExamples(lang: Language?): List<String> {
        return listOf(
                "fold",
                "foam",
                "fo",
                "i will fold",
                "sold",
                "phone",
                "gold",
                "hold",
                "cold"
        )
    }
}

class Stand : Intent() {
    override fun getExamples(lang: Language?): List<String> {
        return listOf(
                "stand",
                "I will stand",
                "I want to stand",
                "stand like a mountain",
                "Steins",
                "Stanley",
                "style"
        )
    }
}

class Double : Intent() {
    override fun getExamples(lang: Language?): List<String> {
        return listOf(
                "double",
                "I will double",
                "I want to double",
                "double me up, Scotty!"
        )
    }
}

class Rules: Intent() {
    override fun getExamples(lang: Language?): List<String> {
        return listOf(
                "rules",
                "what are the rules",
                "read the rules"
        )
    }
}

class WhoAreYou : Intent() {
    override fun getExamples(lang: Language?): List<String> {
        return listOf(
                "who are you?",
                "tell me about yourself"
        )
    }
}

class Exit : Intent() {
    override fun getExamples(lang: Language?): List<String> {
        return listOf(
                "exit game",
                "exit",
                "quit",
                "goodbye",
                "end game",
                "i don't want to play anymore"
        )
    }
}

class ReadName : Intent() {

    @RecordField
    var name : PersonName? = null

    override fun getExamples(lang: Language?): List<String> {
        return listOf(
                "my name is @name",
                "i'm @name",
                "@name"
        )
    }
}