#!/usr/bin/env bash

TEMPLATE_DIR=bertil/textures/default

for c in `cat cards/cards.txt`
do
   convert "cards/${c}.png" -resize 150x160 "cards/${c}_small.png"
   TARGET_DIR=${TEMPLATE_DIR}_${c}
   rm -rf ${TARGET_DIR}
   cp -r ${TEMPLATE_DIR} ${TARGET_DIR}
   convert ${TEMPLATE_DIR}/skin.jpg cards/${c}_small.png -geometry +454+200 -composite ${TARGET_DIR}/skin.jpg
done







