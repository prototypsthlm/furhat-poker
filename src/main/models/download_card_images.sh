#!/usr/bin/env bash

for c in `cat cards/cards.txt`
do
   wget "https://deckofcardsapi.com/static/img/${c}.png" -P cards
done