## Setup

#### SDK
1. Get [the Furhat SDK](https://myfurhat.com/downloads) (this project was built using version 0.0.17).
2. Import SDK as a roject in IntelliJ IDEA.
3. Choose "import project from external model - Gradle"
4. Check the `Use auto-import` checkbox and finish importing
5. Now you should be able to run the web-ui and test the Virtual Furhat. Run the gradle task "application - run". This should start the webserver. 
6. Go to http://localhost:8080/ and add Speech synthesizer and speech recognition settings.
7. Open the Virtual Furhat app from `/dynamicLibrary/Unity_Mac`
8. You should be able to test it from the web interface now.

#### Poker-skill
1. Create a folder named "skill" in the root level of the SDK project
2. From the `skill` folder, clone the repo - `git clone https://gitlab.com/prototypsthlm/furhat-poker.git`
3. Add the line `include 'skill:furhat-poker'` to the SDK settings.gradle file
4. Rebuild the SDK project.
5. Run the skill `main.kt` and it should start up on your Virtual Furhat (if it fails, check that your Kotlin compiler target JVM version is 1.8)


To run the image generation stuff, you will need CLI for Image Magick and wget:  
`brew install imagemagick wget`

Gotchas:

- `ERROR Skill: 50 - Skill loading error: could not load skill.properties. Exiting... java.io.FileNotFoundException: skill.properties`  
Go to Run --> Edit configurations --> and set Working Directory to the _skill_ working directory